uint16_t BitReverse16(uint16_t value) {
  value = (value & 0xFF00) >> 8 | (value & 0x00FF) << 8;
  value = (value & 0xF0F0) >> 4 | (value & 0x0F0F) << 4;
  value = (value & 0xCCCC) >> 2 | (value & 0x3333) << 2;
  value = (value & 0xAAAA) >> 1 | (value & 0x5555) << 1;

  return value;
}
